# README #

### What is this ? ###

* eGain EIM/WIM and Solutions Plus gadget for Cisco Finesse
* Supports single sign on using either username or agent ID logins

### Deployment ###

1. Edit "egain-sso/config.js"; replace the eGain server URL with the base eGain login URL you would use to log in to your eGain product outside of Finesse. Most likely you only need to replace "egain1" with the FQDN of your eGain application server. 

1. Create a copy of the Finesse and (minified) jQuery javascript libraries for your Finesse version and name the files "finesse.js" and "jquery.min.js". If you don't have a copy of these, you can download them from https://developer.cisco.com/site/finesse/downloads/sample-gadgets/

1. Copy these two javascript files and the "egain-sso" folder from this project to the "files" folder on your Finesse servers using the 3rdpartygadget SFTP user. 

1. Add the gadget to your desktop layout in Finesse administration using the gadget URL "/3rdpartygadget/files/egain-sso/gadget.xml".

### Who do I talk to? ###

* My email address is ccondry@cisco.com, but if you need support I recommend the Cisco DevNet forums.

### License ###

The MIT License (MIT)

Copyright (c) 2015 Coty Condry

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
